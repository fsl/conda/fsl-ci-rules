###############################################################################
# This file defines rules for deploying conda packages for FSL projects. It
# is included by the main fsl-ci-rules/.gitlab-ci.yml configuration file.
#
# The rules defined in this file are dependent on the rules defined in
# fsl-ci-build-rules.yml and  fsl-ci-test-rules.yml.
###############################################################################


# Deploy one or more built conda packages to a locally
# accessible conda channel directory. This job is
# created when commits are added to the master branch
# of a recipe repository, or may be programmatically
# triggered by the trigger-package-build job when
# commits are added to the master branch of the project
# repository. In either case, this job must be manually
# started via the gitlab UI.
.deploy-conda-packages:
  stage:    fsl-ci-deploy
  extends: .fsl-ci-rules-job
  tags:
    - fslconda-channel-host

  # We don't need a full python environment
  # to run the deploy script - see
  # rules/fsl-ci-job-template.yml.
  variables:
    SKIP_CREATE_ENV: 1

  rules:

    # Packages can only be published from branches
    # named "master", "main" or "maint/<release-series>",
    # (in which case they will be published to the
    # public channel), or from staging/dev builds
    # (in which case they will be published to the
    # development channel).
    - if: '$FSLCONDA_RECIPE     != null                  &&
           ($CI_COMMIT_REF_NAME == "master"              ||
            $CI_COMMIT_REF_NAME == "main"                ||
            $CI_COMMIT_REF_NAME =~ /^maint\/.*/          ||
            $DEVRELEASE         == "true")               &&
           $CI_PIPELINE_SOURCE  != "merge_request_event" &&
           $FSL_CI_SKIP_ALL     == null'
      when: manual

  script:
    - |
      CONDADIR=/usr/local/conda-build_env/fsl_build/bin
      git clone -b ${FSL_CI_RULES_REVISION} ${FSL_CI_RULES_REPOSITORY} ci-rules;
      PATH=${CONDADIR}:${PATH} PYTHONPATH=$(pwd)/ci-rules python ci-rules/fsl_ci/scripts/deploy_conda_package.py;


# Deploy a single conda package, disabling
# jobs for irrelevant platforms.
.deploy-one-conda-package:
  extends: .deploy-conda-packages
  rules:
    # Do not run jobs if they are disabled
    # via the FSLCONDA_SKIP_PLATFORM variable.
    - if: '($CI_JOB_NAME            == "deploy-noarch-conda-package"             &&
            $FSLCONDA_SKIP_PLATFORM =~ /(^| )noarch($| )/)                       ||
           ($CI_JOB_NAME            == "deploy-linux-64-conda-package"           &&
            $FSLCONDA_SKIP_PLATFORM =~ /(^| )linux-64($| )/)                     ||
           ($CI_JOB_NAME            == "deploy-linux-aarch64-conda-package"      &&
            $FSLCONDA_SKIP_PLATFORM =~ /(^| )linux-aarch64($| )/)                ||
           ($CI_JOB_NAME            == "deploy-macos-64-conda-package"           &&
            $FSLCONDA_SKIP_PLATFORM =~ /(^| )macos-64($| )/)                     ||
           ($CI_JOB_NAME            == "deploy-macos-M1-conda-package"           &&
            $FSLCONDA_SKIP_PLATFORM =~ /(^| )macos-M1($| )/)                     ||
           ($CI_JOB_NAME            =~ /^deploy-linux-64-cuda-.*-conda-package$/ &&
            $FSLCONDA_SKIP_PLATFORM =~ /(^| )linux-64-cuda($| )/)'
      when: never

    # This rule is specifically for CUDA
    # packages. FSL_CONDA_CUDA_VERSION
    # variable controls which CUDA
    # versions are built/deployed.
    # https://gitlab.com/gitlab-org/gitlab/-/issues/35438#note_381577585
    - if:  '$CUDA_VER !~ $FSLCONDA_CUDA_VERSION'
      when: never

    # YAML lists are not merged when using the
    # GitLab CI YAML extends feature, so we
    # have to repeat this rule.
    - if: '$FSLCONDA_RECIPE     != null                                            &&
           ($CI_COMMIT_REF_NAME == "master"                                        ||
            $CI_COMMIT_REF_NAME == "main"                                          ||
            $CI_COMMIT_REF_NAME =~ /^maint\/.*/                                    ||
            $DEVRELEASE         == "true")                                         &&
           $CI_PIPELINE_SOURCE  != "merge_request_event"                           &&
           $FSL_CI_SKIP_ALL     == null                                            &&
           (($CI_JOB_NAME             == "deploy-noarch-conda-package"             &&
             $FSLCONDA_BUILD_PLATFORM =~ /(^| )noarch($| )/)                       ||
            ($CI_JOB_NAME             == "deploy-linux-64-conda-package"           &&
             $FSLCONDA_BUILD_PLATFORM =~ /(^| )linux-64($| )/)                     ||
            ($CI_JOB_NAME             == "deploy-linux-aarch64-conda-package"      &&
             $FSLCONDA_BUILD_PLATFORM =~ /(^| )linux-aarch64($| )/)                ||
            ($CI_JOB_NAME             == "deploy-macos-64-conda-package"           &&
             $FSLCONDA_BUILD_PLATFORM =~ /(^| )macos-64($| )/)                     ||
            ($CI_JOB_NAME             == "deploy-macos-M1-conda-package"           &&
             $FSLCONDA_BUILD_PLATFORM =~ /(^| )macos-M1($| )/)                     ||
            ($CI_JOB_NAME             =~ /^deploy-linux-64-cuda-.*-conda-package$/ &&
             $FSLCONDA_BUILD_PLATFORM =~ /(^| )linux-64-cuda($| )/))'
      when: manual



deploy-all-conda-packages:
  extends: .deploy-conda-packages
  dependencies:
    - build-noarch-conda-package
    - build-linux-64-conda-package
    - build-linux-aarch64-conda-package
    - build-macos-64-conda-package
    - build-macos-M1-conda-package
    - build-linux-64-cuda-11.0-conda-package


deploy-noarch-conda-package:
  extends: .deploy-one-conda-package
  dependencies:
    - build-noarch-conda-package


deploy-linux-64-conda-package:
  extends: .deploy-one-conda-package
  dependencies:
    - build-linux-64-conda-package


deploy-linux-aarch64-conda-package:
  extends: .deploy-one-conda-package
  dependencies:
    - build-linux-aarch64-conda-package


deploy-macos-64-conda-package:
  extends: .deploy-one-conda-package
  dependencies:
    - build-macos-64-conda-package


deploy-macos-M1-conda-package:
  extends: .deploy-one-conda-package
  dependencies:
    - build-macos-M1-conda-package


deploy-linux-64-cuda-11.0-conda-package:
  extends: .deploy-one-conda-package
  dependencies:
    - build-linux-64-cuda-11.0-conda-package
  variables:
    CUDA_VER: "11.0"


# Identify other packages which depend on this one,
# check to see if their conda recipe pins the
# version of this package, and open a MR to update
# that version to the latest.
update-dependant-recipes:
  stage:   fsl-ci-deploy
  image:   $FSL_CI_IMAGE_LINUX_64
  extends: .fsl-ci-rules-job
  tags:
    - fsl-ci
    - docker

  # This job is only run on new stable builds
  # of a package
  rules:
    - if: '$FSLCONDA_RECIPE    != null     &&
           $CI_COMMIT_REF_NAME == "master" &&
           $FSL_CI_SKIP_ALL    == null'
      when: manual

  script:
    - update_dependant_recipes
