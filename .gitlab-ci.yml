###############################################################################
# This file defines the default CI configuration rules for all FSL projects.
# Refer to README.md for an overview.
###############################################################################


# If your project has its own .gitlab-ci.yml file, it should include
# this file, and add all of these stages to its list of stages.
stages:
  - fsl-ci-pre
  - fsl-ci-build
  - fsl-ci-test
  - fsl-ci-deploy


# These variables need to configured so that CI jobs can upload and download
# to/from the FSL conda channels.
variables:

  # URL to this repository - All CI jobs clone it,
  # so that they have access to various scripts and
  # utilities contained in the scripts directory.
  FSL_CI_RULES_REPOSITORY: https://git.fmrib.ox.ac.uk/fsl/conda/fsl-ci-rules.git
  FSL_CI_RULES_REVISION:   main

  # URL to the FSL public, dev, and internal conda channels,
  # where FSL conda packages are available for download.
  FSLCONDA_PUBLIC_CHANNEL_URL:      http://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public
  FSLCONDA_INTERNAL_CHANNEL_URL:    http://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/internal
  FSLCONDA_DEVELOPMENT_CHANNEL_URL: http://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/development

  # Login credentials for accessing the internal
  # conda channel - these variables must be set
  # via the GitLab web UI.
  FSLCONDA_INTERNAL_CHANNEL_USERNAME: username
  FSLCONDA_INTERNAL_CHANNEL_PASSWORD: password

  # Paths to conda channel directories, which are assumed
  # to be configured to be locally accessible to deploy jobs
  FSLCONDA_PUBLIC_CHANNEL_DIRECTORY:      /exports/fsldownloads/fslconda/public
  FSLCONDA_INTERNAL_CHANNEL_DIRECTORY:    /exports/fsldownloads/fslconda/internal
  FSLCONDA_DEVELOPMENT_CHANNEL_DIRECTORY: /exports/fsldownloads/fslconda/development

  # Docker images to use for platform specific
  # builds. The LINUX_64 image is also used
  # for platform-agnostic jobs. These images
  # are built using jobs defined within the
  # rules/fsl-ci-management-rules.yml file.
  FSL_CI_IMAGE_LINUX_64:           fsldevelopment/fsl-almalinux-64
  FSL_CI_IMAGE_LINUX_64_CUDA_11_0: fsldevelopment/fsl-almalinux-64-cuda-11.0
  FSL_CI_IMAGE_LINUX_AARCH64:      fsldevelopment/fsl-almalinux-aarch64

  # Variables used for building CUDA packages. The
  # CUDA_VER variable is set in the docker images,
  # and the FSLCONDA_CUDA_VERSION variable is set
  # as a gitlab CI / CD variable in the CUDA
  # package conda recipe repositories.
  CUDA_VER              : ""
  FSLCONDA_CUDA_VERSION : ""

  # We currently use m4.large EC2 instances, which
  # have a single core, and support 2 threads per core
  # https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/cpu-options-supported-instances-values.html
  MAKEFLAGS: "-j 2"

  # The following variables are assumed to be set via the gitlab
  # web interface on all FSL project and recipe repositories:
  #
  #   - FSL_CI_API_TOKEN: Gitlab API access token which provides read/write
  #     API access.
  #
  #   - FSLCONDA_RECIPE_URL: Only required on the project repository if
  #     the accompanying recipe repository is not named
  #     "fsl/conda/fsl-<project>", or if a project is associated with more
  #     than one recipe repository.
  #
  #   - FSLCONDA_RECIPE: Must be set on all recipe repositories, as it is
  #     used to distinguish between project and recipe repositories.


# All CI jobs are included from sub-files here.
include:
  - local: /rules/fsl-ci-job-template.yml
  - local: /rules/fsl-ci-management-rules.yml
  - local: /rules/fsl-ci-build-rules.yml
  - local: /rules/fsl-ci-test-rules.yml
  - local: /rules/fsl-ci-deploy-rules.yml
