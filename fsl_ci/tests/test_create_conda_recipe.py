#!/usr/bin/env python
#
# test_create_conda_recipe.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import                   os
import os.path    as     op
import itertools  as     it
import contextlib as     ctxlib
import                   shlex
import                   difflib
import                   shutil
from   unittest   import mock
import jinja2     as     j2

import fsl_ci.utils.create_conda_recipe as create_conda_recipe

from fsl_ci.gitlab import get_project_version
from fsl_ci        import tempdir, indir


BENCHMARK_DIR = op.join(op.dirname(__file__), 'recipes')


GITLAB_URL = 'https://git.fmrib.ox.ac.uk'


def token():
    """A GitLab API token needs to be provided by the environment. """
    candidates = ['FSL_CI_API_TOKEN', 'CI_JOB_TOKEN', 'TOKEN']
    for c in candidates:
        if c in os.environ:
            # some tokens start with "-", which will
            # trip up argparse, so we quote and pad
            # with a space
            return f'" {os.environ[c]}"'

    raise RuntimeError(f'One of {candidates} must be set!')


@ctxlib.contextmanager
def environment():
    env = os.environ.copy()
    env.pop('FSLDIR', None)
    with mock.patch('os.environ', env):
        yield


def compare_directories(project, benchmarkdir, directory):
    tkn       = token().strip('"').strip()
    basever   = get_project_version('fsl/base', GITLAB_URL, tkn)
    projver   = get_project_version(project,    GITLAB_URL, tkn)
    tvars     = {'fsl_base_version' : basever, 'project_version' : projver}
    directory = op.abspath(directory)
    with tempdir():
        shutil.copytree(benchmarkdir, 'benchmark')
        benchmarkdir = op.abspath('benchmark')
        with indir(benchmarkdir):
            filenames = []
            for dirpath, _, dirfiles in os.walk('.'):

                if '.git' in dirpath:
                    continue
                for fname in dirfiles:
                    loader   = j2.FileSystemLoader('.')
                    env      = j2.Environment(loader=loader,
                                              block_start_string='@=',
                                              block_end_string='=@',
                                              variable_start_string='@@',
                                              variable_end_string='@@')
                    template = env.get_template(fname)
                    with open(fname, 'wt') as f:
                        f.write(template.render(**tvars))

                filenames.extend([op.join(dirpath, f) for f in dirfiles])

        for filename in filenames:
            with open(op.join(benchmarkdir, filename), 'rt') as f: exp = f.read()
            with open(op.join(directory,    filename), 'rt') as f: got = f.read()
            assert exp.strip() == got.strip()


def test_create_cpp_conda_recipe():
    with tempdir(), environment():
        args = (f'-t {token()} recipe '
                '-rp fsl/conda/fsl-newimage -pp fsl/newimage')
        create_conda_recipe.main(shlex.split(args))
        compare_directories('fsl/newimage',
                            op.join(BENCHMARK_DIR, 'cpp'),
                            'recipe')


def test_create_tcl_conda_recipe():
    with tempdir(), environment():
        args = (f'-t {token()} recipe '
                '-rp fsl/conda/fsl-feat5 -pp fsl/feat5')
        create_conda_recipe.main(shlex.split(args))
        compare_directories('fsl/feat5',
                            op.join(BENCHMARK_DIR, 'tcl'),
                            'recipe')


def test_create_cuda_conda_recipe():
    with tempdir(), environment():
        args = (f'-t {token()} recipe -pt cuda '
                '-rp fsl/conda/fsl-ptx2 -pp fsl/ptx2')
        create_conda_recipe.main(shlex.split(args))
        compare_directories('fsl/ptx2',
                            op.join(BENCHMARK_DIR, 'cuda'),
                            'recipe')


def test_create_other_conda_recipe():
    with tempdir(), environment():
        args = (f'-t {token()} recipe '
                '-rp fsl/conda/fsl-misc_scripts -pp fsl/misc_scripts')
        create_conda_recipe.main(shlex.split(args))
        compare_directories('fsl/misc_scripts',
                            op.join(BENCHMARK_DIR, 'other'),
                            'recipe')


def test_create_python_conda_recipe():
    with tempdir(), environment():
        args = (f'-t {token()} recipe '
                '-rp fsl/conda/fsl-eddy_qc -pp fsl/eddy_qc')
        create_conda_recipe.main(shlex.split(args))
        compare_directories('fsl/eddy_qc',
                            op.join(BENCHMARK_DIR, 'python'),
                            'recipe')
