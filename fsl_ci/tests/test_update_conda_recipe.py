#!/usr/bin/env python
#
# test_update_conda_recipe.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import             os
import os.path  as op
import textwrap as tw

from fsl_ci         import  tempdir
from fsl_ci.recipe  import (create_python_recipe_template,
                            get_recipe_variable)
from fsl_ci.scripts import  update_conda_recipe

from fsl_ci.tests import create_mock_python_project


def get_link_script_executables(script):

    if 'pre' in script: key = 'removeFSLWrapper'
    else:               key = 'createFSLWrapper'

    with open(script) as f:
        line = f.readlines()[1]

    if key in line:
        return sorted(line.split()[1:])
    return []


def test_update_conda_recipe():


    with tempdir():
        os.mkdir('project')
        os.mkdir('recipe')
        entrypoints, scripts = create_mock_python_project('project', 1)
        exes                 = [ep.split('=')[0].strip() for ep in entrypoints]
        create_python_recipe_template('recipe',
                                      'fsl/project',
                                      'fsl/conda/fsl-project',
                                      'git.fmrib',
                                      '1.0.0',
                                      '0',
                                      entrypoints,
                                      exes)

        with open('recipe/meta.yaml') as f:
            meta = f.read()

        assert get_recipe_variable(meta, 'build')   == '0'
        assert get_recipe_variable(meta, 'version') == '1.0.0'

        create_mock_python_project('project', 2)

        update_conda_recipe.update_recipe('project', 'recipe', '2.0.0', '1')

        with open('recipe/meta.yaml') as f:
            meta = f.read()

        assert get_recipe_variable(meta, 'build')   == '1'
        assert get_recipe_variable(meta, 'version') == '2.0.0'
