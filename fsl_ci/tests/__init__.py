#!/usr/bin/env python


import textwrap as tw
import os.path  as op


def create_mock_python_project(project_dir, nentrypoints, nscripts=0):

    entrypoints = []
    for i in range(nentrypoints):
        entrypoints.append(f'"entrypoint{i} = pkg.entrypoint{i}:main"')

    scripts = []
    for i in range(nscripts):
        scripts.append(f'"pkg/script{i}"')

    setuppy = tw.dedent(f"""
    from setuptools import setup
    setup(
        name="project",
        version="1.0.0",
        entry_points={{'console_scripts' : [{','.join(entrypoints)}]}},
        scripts=[{','.join(scripts)}]
    )
    """)

    with open(op.join(project_dir, 'setup.py'), 'wt') as f:
        f.write(setuppy)

    entrypoints = [ep.strip('"') for ep in entrypoints]
    scripts     = [s .strip('"') for s  in scripts]

    return entrypoints, scripts
