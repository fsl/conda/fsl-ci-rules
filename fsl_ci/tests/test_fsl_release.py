#!/usr/bin/env python


import textwrap as tw

import yaml

from fsl_ci.fsl_release import get_build_package_versions


def test_get_build_package_versions():
    old_format = tw.dedent("""
    base-packages:
      - a
      - b
      - c

    packages:
      - a 1.2
      - c 3.4 *build
      - d 4.5

    linux-64-packages:
     - b 2.3
    macos-M1-packages:
     - b 6.7
    macos-64-packages: []
    """).strip()
    old_expect_linux_64 = {'a' : '1.2', 'b' : '2.3', 'c' : '3.4 *build',
                           'liblapack' : '3.8 *netlib',
                           'libblas'   : '3.8 *netlib'}
    old_expect_macos_M1 = {'a' : '1.2', 'b' : '6.7', 'c' : '3.4 *build',
                           'liblapack' : '3.9 *netlib',
                           'libblas'   : '3.9 *netlib'}

    new_format = tw.dedent("""
    build-packages:
      - a 5.6
      - b
      - c
      - linux-64:
        - a 6.7
        - b 8.9
        macos-64:
        - a 6.8
    packages:
      - a 1.2
      - b 2.3
      - c 3.4 *build
      - d 4.5
    """)

    new_expect_linux_64 = {'a' : '6.7', 'b' : '8.9', 'c' : '3.4 *build'}
    new_expect_macos_64 = {'a' : '6.8', 'b' : '2.3', 'c' : '3.4 *build'}
    new_expect_macos_M1 = {'a' : '5.6', 'b' : '2.3', 'c' : '3.4 *build'}

    old_yml = yaml.load(old_format, Loader=yaml.Loader)
    new_yml = yaml.load(new_format, Loader=yaml.Loader)

    assert get_build_package_versions(old_yml, 'linux-64') == old_expect_linux_64
    assert get_build_package_versions(old_yml, 'macos-M1') == old_expect_macos_M1
    assert get_build_package_versions(new_yml, 'linux-64') == new_expect_linux_64
    assert get_build_package_versions(new_yml, 'macos-64') == new_expect_macos_64
    assert get_build_package_versions(new_yml, 'macos-M1') == new_expect_macos_M1
