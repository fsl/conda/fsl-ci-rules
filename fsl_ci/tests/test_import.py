#!/usr/bin/env python


def test_import():
    """Rudimentary health check of the fsl-ci-rules codebase. """
    import fsl_ci.conda
    import fsl_ci.fsl
    import fsl_ci.fsl_release
    import fsl_ci.gitlab
    import fsl_ci.recipe
    import fsl_ci.platform
    import fsl_ci.scripts.build_conda_package
    import fsl_ci.scripts.clear_channel
    import fsl_ci.scripts.deploy_conda_package
    import fsl_ci.scripts.purge_channel_indexes
    import fsl_ci.scripts.run_unit_tests
    import fsl_ci.scripts.trigger_package_build
    import fsl_ci.scripts.update_conda_recipe
    import fsl_ci.scripts.update_dependant_recipes
    import fsl_ci.utils.build_local_conda_package
    import fsl_ci.utils.configure_repositories
    import fsl_ci.utils.create_conda_recipe
    import fsl_ci.utils.fsl_project_dependencies
    import fsl_ci.utils.full_rebuild
    import fsl_ci.utils.git_lfs_clone
    import fsl_ci.utils.package_status
    import fsl_ci.utils.set_gitlab_variables
    import fsl_ci.utils.trigger_build
    import fsl_ci.utils.trigger_pipeline
