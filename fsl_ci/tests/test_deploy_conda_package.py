#!/usr/bin/env python
#
# test_deploy_conda_package.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import os
import os.path as op

from pathlib import Path

import pytest

from fsl_ci import tempdir

from fsl_ci.scripts.deploy_conda_package import copy_packages


def test_copy_packages():

    platforms = ['noarch', 'linux-64', 'osx-64', 'osx-arm64']

    with tempdir(False) as blddir, \
         tempdir(False) as chandir:

        platblddirs  = [Path(blddir)  / f'_{p}_' / p for p in platforms]
        platchandirs = [Path(chandir) /            p for p in platforms]

        for plat, platblddir in zip(platforms, platblddirs):
            platblddir.mkdir(parents=True)
            pkgfile = platblddir / f'{plat}-some-package.tar.bz2'
            pkgfile.touch()

        copy_packages(blddir, chandir, False)

        for plat, platchandir in zip(platforms, platchandirs):
            pkgfile = platchandir / f'{plat}-some-package.tar.bz2'
            assert list(platchandir.iterdir()) == [pkgfile]

        with pytest.raises(RuntimeError):
            copy_packages(blddir, chandir, False)

        copy_packages(blddir, chandir, True)
