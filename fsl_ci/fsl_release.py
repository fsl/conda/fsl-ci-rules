#!/usr/bin/env python
"""Functions for working with the fsl-release.yml file, which is hosted
in the fsl/conda/manifest git repository.
"""

import              copy
import itertools as it

import yaml

from typing import Dict

from fsl_ci.gitlab import download_file


def download_fsl_release(server : str,
                         token  : str,
                         repo   : str = None,
                         branch : str = None) -> dict:
    """Download and load the fsl-release.yml file from the fsl/conda/manifest
    repository.
    """
    if repo is None:
        repo = 'fsl/conda/manifest'
    if branch is None:
        branch = 'main'

    rel = download_file(repo, 'fsl-release.yml', server, token, branch)
    return yaml.load(rel, Loader=yaml.Loader)


def get_build_package_versions(
        fslrelease : dict,
        platform   : str) -> Dict[str, str]:
    """Extracts all core/base packages and their versions from the
    fsl-release.yml information, for pinning at package build time.

    :arg fslrelease: Dictionary containing contents of fsl-release.yml
    :arg platform:   Platform identifier, e.g. "macos-64", "linux-64".
    """

    fslrelease = copy.deepcopy(fslrelease)

    # Legacy fsl-release.yml format, used prior to
    # FSL 6.0.6.5, and to the merging of fsl/conda/manifest!183
    if 'base-packages' in fslrelease:
        basepkgs = get_base_package_versions(fslrelease, platform)
        # HACK which emulates the build configuration for
        # FSL <= 6.0.6.4. Prior to this version, the
        # liblapack and libblas versions were hard-coded
        # in fsl_ci/templates/conda_build_config.yaml
        if platform == 'macos-M1':
            basepkgs['liblapack'] = '3.9 *netlib'
            basepkgs['libblas']   = '3.9 *netlib'
        else:
            basepkgs['liblapack'] = '3.8 *netlib'
            basepkgs['libblas']   = '3.8 *netlib'
        return basepkgs

    # Package entries are of the form:
    #
    #   "pkgname [version [build]]"
    #
    # We turn this into a dict of {pkgname : pkgver} mappings,
    # where pkgver is either:
    #    - None
    #    - "version"
    #    - "version build"
    def parse_package_list(pkglist):
        pkglist = [p.split(maxsplit=1)     for p               in pkglist]
        pkglist = [(p[0], p[1:] or [None]) for p               in pkglist]
        return    {pkgname : pkgver[0]     for pkgname, pkgver in pkglist}

    # packages contains a list of all
    # packages (including base packages)
    # and their versions
    allpkgs = fslrelease['packages']

    # Incorporate platform specific lists
    # into main packages list
    if f'{platform}-packages' in fslrelease:
        allpkgs = allpkgs + fslrelease[f'{platform}-packages']

    # base packages contains a list
    # of all base/core dependencies.
    buildpkgs = fslrelease['build-packages']

    # There may be a dictionary in
    # build-packages, containing
    # platform-specific packages/pins
    platpkgs = {}
    for i, pkg in enumerate(list(buildpkgs)):
        if isinstance(pkg, dict):
            platpkgs = buildpkgs.pop(i)
            break
    platpkgs = platpkgs.get(platform, [])

    # Create a dict of {pkg:ver} mappings
    # for build packages. Versions may be
    # sourced from any of these, in oreder
    # of precedence:
    #   - platform-specific build-packages list
    #   - build-packages list
    #   - packages list
    platpkgs  = parse_package_list(platpkgs)
    buildpkgs = parse_package_list(buildpkgs)
    allpkgs   = parse_package_list(allpkgs)
    pkgvers   = {}

    # All packages listed in build-packages,
    # and platform-specific build-packages. The
    # latter is considered last, so that it
    # will overwrite versions in the former.
    for pkg in it.chain(buildpkgs.keys(), platpkgs.keys()):
        platver  = platpkgs. get(pkg, None)
        buildver = buildpkgs.get(pkg, None)
        pkgsver  = allpkgs.  get(pkg, None)

        if   platver  is not None: pkgvers[pkg] = platver
        elif buildver is not None: pkgvers[pkg] = buildver
        elif pkgsver  is not None: pkgvers[pkg] = pkgsver

        # this will only happen if an unversioned
        # package is listed in build-packages,
        # and is not in the packages list - likely
        # an oversight.
        else:                      pkgvers[pkg] = None

    return pkgvers


def get_base_package_versions(
        fslrelease : dict,
        platform   : str) -> Dict[str, str]:
    """Extracts all core/base packages and their versions from the
    fsl-release.yml information.

    This function is used to parse versions of the fsl-release.yml file
    prior to FSL 6.0.6.5 (see fsl/conda/manifest!183). The base-packages
    list was removed from fsl-release.yml in that MR, and replaceed with
    "build-packages".

    :arg fslrelease: Dictionary containing contents of fsl-release.yml
    :arg platform:   Platform identifier, e.g. "macos-64", "linux-64".
    """

    # base packages contains a list
    # of all base/core dependencies.
    basepkgs = fslrelease['base-packages']

    # packages contains a list of all
    # packages (including base packages)
    # and their versions
    allpkgs  = fslrelease['packages']

    if   'linux-64' in platform:
        allpkgs = allpkgs + fslrelease['linux-64-packages']
    if   'linux-aarch64' in platform:
        allpkgs = allpkgs + fslrelease['linux-aarch64-packages']
    elif 'macos-64' in platform:
        allpkgs = allpkgs + fslrelease['macos-64-packages']
    elif 'macos-M1' in platform:
        allpkgs = allpkgs + fslrelease['macos-M1-packages']

    # Entries are of the form "pkgname [version [build]]"
    # Turn this into a dict of {pkgname : pkgver} mappings,
    # where pkgver is either:
    #    - None
    #    - "version"
    #    - "version build"
    allpkgs = [p.split(maxsplit=1)     for p               in allpkgs]
    allpkgs = [(p[0], p[1:] or [None]) for p               in allpkgs]
    allpkgs = {pkgname : pkgver[0]     for pkgname, pkgver in allpkgs}
    return {pkg : allpkgs.get(pkg, None) for pkg in basepkgs}
