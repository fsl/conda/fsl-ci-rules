#!/usr/bin/env python
#
# recipe.py - Functions for creating and manipulating FSL project conda
#             recipes.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import              copy
import              io
import itertools as it
import              re
import              os
import os.path   as op

from typing import List, Tuple, Optional

try:
    import jinja2 as j2
    import           ruamel.yaml as yaml
except Exception:
    j2   = None
    yaml = None

from fsl_ci.gitlab import gen_repository_url


TEMPLATE_DIR = op.abspath(op.join(op.dirname(__file__), 'templates'))
"""Directory which contains conda recipe template files. """


def get_recipe_variable(text : str, varname : str) -> Optional[str]:
    """Return the value of a variable. Searches in text for a line of the form:

        {% set varname = 'value' %}

    and returns the value. Returns None if the variable cannot be found.

    Includes a hack to accommodate FSL CUDA projects - for these projects,
    different packages are generated from the same conda recipe for different
    CUDA versions, with the resulting packages named <package>-cuda-X.Y. For
    these recipes, the CUDA_VER environment variable is queried to determine
    the appropriate CUDA version.
    """
    pat   = r'{{% +set +{} += +[\'"](.*)[\'"] *(\+ *cuda_label)? *%}}'\
        .format(varname)
    lines = list(text.split('\n'))

    cudaver = os.environ.get('CUDA_VER', None)

    for line in lines:
        match = re.fullmatch(pat, line.strip())
        if match:
            name, iscuda = match.groups()

            if   not iscuda:          return name
            elif cudaver is not None: return name + '-cuda-' + cudaver
            else:                     return name + '-cuda-X.Y'

    return None


def update_recipe_variable(text : str, varname : str, newvalue : str) -> str:
    """Gives a variable a new value. Searches in text for a line of the form:

        {% set varname = 'oldvalue' %}

    and replaces it with:

        {% set varname = 'newvalue' %}

    Returns the patched text.
    """

    pat   = r'{{% +set +{}( +)=( +)[\'"](.*)[\'"] +%}}'.format(varname)
    lines = list(text.split('\n'))

    for i, line in enumerate(lines):
        match = re.fullmatch(pat, line.strip())
        if match:
            space1   = match.group(1)
            space2   = match.group(2)
            lines[i] = "{{% set {}{}={}'{}' %}}".format(
                varname, space1, space2, newvalue)
            break

    return '\n'.join(lines)


def update_recipe_dependency(text : str, depname : str, newver : str) -> str:
    """Updates the version of specified dependency/requirement depname to
    newver.
    """

    lines = text.split('\n')
    pat   = rf'^( +)- *{depname} '

    for i, line in enumerate(lines):
        match = re.match(pat, line)
        if match:
            space    = match.group(1)
            lines[i] = f'{space}- {depname} {newver}'
            break

    return '\n'.join(lines)


def patch_recipe_version(text : str, new_version : str) -> str:
    """Patches the package version number in the recipe metadata.

    The version variable (see update_recipe_variable) is not changed,
    as this is also used as the git revision to build.
    """

    pat   = r'^( +)version:.*$'
    lines = list(text.split('\n'))

    for i, line in enumerate(lines):
        match = re.fullmatch(pat, line)
        if match:
            space    = match.group(1)
            lines[i] = f'{space}version: {new_version}'
            break

    return '\n'.join(lines)


def render_template(template_file, dest_file=None, **env):
    """Loads and renders a jinja2 template file with the given environment,
    optionally saving it to the given destination.
    """
    template_dir = op.dirname(template_file)
    loader       = j2.FileSystemLoader(op.dirname(template_dir))
    jenv         = j2.Environment(loader=loader,
                                  trim_blocks=True,
                                  lstrip_blocks=True)

    with open(template_file, 'rt') as f:
        template = jenv.from_string(f.read())

    rendered = template.render(**env)

    if dest_file is not None:
        with open(dest_file, 'wt') as f:
            f.write(rendered)

    return rendered


def patch_meta_yaml_file(base_file, dest, patch_files, env=None):
    """Merges additional metadata from a set of "patch" files into a
    primary meta.yaml file. The secondary files are loaded as jinja2
    templates with the given environment.
    """

    if env is None:
        env = {}
    env = dict(env)

    def ensure(dic, field, ftype):
        if field not in dic:
            dic[field] = ftype()
        return dic[field]

    yam = yaml.YAML(typ='jinja2')
    yam.indent(mapping=2, sequence=4, offset=2)
    with open(base_file, 'rt') as f:
        base = yam.load(f.read())

    all_builds = [base] + base.get('outputs', [])

    for build, patch_file in it.product(all_builds, patch_files):

        def has_requirement(pkg, rtype):
            reqs = build.get('requirements', {}).get(rtype, [])
            return any(pkg in req for req in reqs)
        env['has_requirement'] = has_requirement

        patch = yam.load(render_template(patch_file, None, **env))
        if patch is None:
            continue

        # custom merge rules

        # Append any requirements from template
        # file into main file requirements
        if 'requirements' in patch and 'requirements' in build:
            basereqs  = ensure(build, 'requirements', dict)
            patchreqs = patch['requirements']
            for sect in ('host', 'build', 'run'):
                if sect not in patchreqs:
                    continue
                basesect = ensure(basereqs, sect, list)
                newreqs  = patchreqs[sect]
                print(f'Inserting additional {sect} '
                      f'requirements: {", ".join(newreqs)}')
                basesect.extend(newreqs)

        # Add/clobber any build settings from the
        # template file into the main file.
        if 'build' in patch and 'build' in build:
            basebuild  = ensure(build, 'build', dict)
            for k, v in patch['build'].items():
                if k in basebuild: action = 'Overwriting'
                else:              action = 'Inserting'
                print(f'{action} build/{k}: {v}')
                basebuild[k] = v

    s = io.StringIO()
    yam.dump(base, s)
    base = s.getvalue()

    if dest is not None:
        with open(dest, 'wt') as f:
            f.write(base)

    return base


def create_link_scripts(recipe_dir  : str,
                        executables : List[str],
                        tcl         : bool = False):
    """Creates post-link.sh and pre-unlink.sh scripts.

    In an official FSL installation, wrapper scripts are created for every FSL
    executable, so they can be isolated from the rest of the environment.
    These wrapper scripts are created/removed via the post-link.sh and
    pre-unlink.sh scripts.
    """

    import jinja2 as j2

    if len(executables) == 0:
        return

    postlink_template  = op.join(TEMPLATE_DIR, 'post-link.sh.template')
    preunlink_template = op.join(TEMPLATE_DIR, 'pre-unlink.sh.template')
    postlink_dest      = op.join(recipe_dir,   'post-link.sh')
    preunlink_dest     = op.join(recipe_dir,   'pre-unlink.sh')

    templates = [(postlink_template,  postlink_dest),
                 (preunlink_template, preunlink_dest)]

    template_env = {'exes' : executables, 'tcl' : tcl}

    for template, dest in templates:
        with open(template, 'rt') as f:
            loader   = j2.FileSystemLoader(TEMPLATE_DIR)
            env      = j2.Environment(loader=loader,
                                      trim_blocks=True,
                                      lstrip_blocks=True)
            template = env.from_string(f.read())
        with open(dest, 'wt') as f:
            f.write(template.render(**template_env) + '\n')


def create_python_recipe_template(
        recipe_dir     : str,
        project_path   : str,
        recipe_path    : str,
        server         : str,
        version        : str,
        build          : str,
        entrypoints    : List[str],
        exes           : List[str]):
    """Creates a conda recipe for a Python project, storing the recipe files
    in recipe_dir.

    :arg recipe_dir:     Directory in which to save generated recipe files.
    :arg project_path:   Project repository path on GitLab
    :arg recipe_path:    Recipe repository path on GitLab
    :arg server:         GitLab server URL
    :arg version:        Project version
    :arg build:          Project build number
    :arg entrypoints:    List of "command = function" strings, specifying
                         Python entrypoints
    :arg exes:           Names of all executables provided by the project,
                         including those in entrypoints.
    """

    import jinja2 as j2

    project_url        = gen_repository_url(project_path, server)
    recipe_url         = gen_repository_url(recipe_path, server)
    recipe_name        = recipe_path.rsplit('/')[-1]
    metayaml_template  = op.join(TEMPLATE_DIR, 'meta.yaml.common.template')
    sub_template       = 'meta.yaml.python.template'
    metayaml_dest      = op.join(recipe_dir,   'meta.yaml')
    template_env       = {
        'template_name'              : recipe_name,
        'template_repository'        : project_url,
        'template_recipe_repository' : recipe_url,
        'sub_template'               : sub_template,
        'template_build'             : build,
        'template_version'           : version,
        'entrypoints'                : entrypoints,
    }

    create_link_scripts(recipe_dir, exes)

    with open(metayaml_template, 'rt') as f:
        loader   = j2.FileSystemLoader(TEMPLATE_DIR)
        env      = j2.Environment(loader=loader,
                                  trim_blocks=True,
                                  lstrip_blocks=True)
        template = env.from_string(f.read())
    with open(metayaml_dest, 'wt') as f:
        f.write(template.render(**template_env) + '\n')


def create_cpp_recipe_template(
        recipe_dir     : str,
        project_path   : str,
        recipe_path    : str,
        server         : str,
        version        : str,
        build          : str,
        exes           : List[str]             = None,
        requirements   : List[Tuple[str, str]] = None,
        ptype          : str                   = 'cpp',
        tcl            : bool                  = False):
    """Creates a conda recipe for a C/C++/CUDA/Other Makefile-based FSL
    project, storing the recipe files in recipe_dir.

    :arg recipe_dir:     Directory in which to save generated recipe files.
    :arg project_path:   Project repository path on GitLab
    :arg recipe_path:    Recipe repository path on GitLab
    :arg server:         GitLab server URL
    :arg version:        Project version
    :arg build:          Project build number
    :arg exes:           Names of all executables provided by the project
    :arg requirements:   List of (package, version) tuples specifying the
                         project dependencies
    :arg project_type:   One of "cpp" (default), "cuda", or "other"
    :arg tcl:            If this project installs TCL scripts.
    """

    import jinja2 as j2

    if exes         is None: exes         = []
    if requirements is None: requirements = []

    project_url        = gen_repository_url(project_path, server)
    recipe_url         = gen_repository_url(recipe_path,  server)
    recipe_name        = recipe_path.rsplit('/')[-1]
    buildsh_template   = op.join(TEMPLATE_DIR, f'build.sh.{ptype}.template')
    metayaml_template  = op.join(TEMPLATE_DIR,  'meta.yaml.common.template')
    sub_template       =                       f'meta.yaml.{ptype}.template'
    buildsh_dest       = op.join(recipe_dir,    'build.sh')
    metayaml_dest      = op.join(recipe_dir,    'meta.yaml')

    if requirements is not None:
        requirements = list(requirements)
        for i, (name, ver) in enumerate(requirements):
            if ver is None:
                ver = ''
            if ver != '':
                try:
                    ver = ver.split('.')[0]
                except Exception:
                    ver = ''

            requirements[i] = (name, ver)

    template_env = {
        'template_name'              : recipe_name,
        'template_repository'        : project_url,
        'template_recipe_repository' : recipe_url,
        'template_version'           : version,
        'template_build'             : build,
        'sub_template'               : sub_template,
        'dependencies'               : requirements,
    }

    create_link_scripts(recipe_dir, exes, tcl)

    with open(buildsh_template, 'rt') as inf, \
         open(buildsh_dest, 'wt') as outf:
        outf.write(inf.read())

    with open(metayaml_template, 'rt') as f:
        loader   = j2.FileSystemLoader(TEMPLATE_DIR)
        env      = j2.Environment(loader=loader,
                                  trim_blocks=True,
                                  lstrip_blocks=True)
        template = env.from_string(f.read())
    with open(metayaml_dest, 'wt') as f:
        f.write(template.render(**template_env) + '\n')


def create_cuda_recipe_template(*args, **kwargs):
    """Creates a conda recipe for a CUDA FSL project, storing the recipe
    files in recipe_dir. See create_cpp_recipe_template.
    """
    create_cpp_recipe_template(*args, **kwargs, ptype='cuda')


def create_other_recipe_template(*args, **kwargs):
    """Creates a conda recipe for a Makefile-based FSL project, storing the
    recipe files in recipe_dir. See create_cpp_recipe_template.
    """
    create_cpp_recipe_template(*args, **kwargs, ptype='other')


def create_unknown_recipe_template(*args, **kwargs):
    """Creates a conda recipe for a FSL project of unknown type, storing the
    recipe files in recipe_dir. See create_cpp_recipe_template.
    """
    create_cpp_recipe_template(*args, **kwargs, ptype='unknown')
