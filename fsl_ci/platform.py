#!/usr/bin/env python
#
# platform.py - Functions for working with FSL package platform identifiers.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import platform
import re
import sys

from typing import List, Tuple


# Note: These lists must be updated whenever new
# platforms / CUDA versions become supported.
COMPILATION_TARGETS = ['linux-64', 'linux-aarch64', 'macos-64', 'macos-M1']
CUDA_VERSIONS       = ['9.2', '10.2', '11.0', '11.1', '11.3', '11.4', '11.5']


def get_recipe_platform(meta : dict) -> str:
    """Inspects the given recipe metadata, and returns one of ``'noarch'``,
    ``'binary'``, or ``'cuda'``, denoting the package type.
    """
    noarch = (('build'  in meta) and
              ('noarch' in meta['build']))

    # CUDA package names end with "-cuda-X.Y"
    cudapat = r'.*-cuda-(\d+\.\d+|X\.Y)'
    cuda    = (('package' in meta)            and
               ('name'    in meta['package']) and
               re.fullmatch(cudapat, meta['package']['name']))

    # The type of package, gleaned
    # from the recipe meta.yaml - one of
    #  - noarch: platform-independent (e.g. python, generic)
    #  - cuda:   Linux+CUDA or macOS+CUDA
    #  - binary: C/C++
    if   noarch: return 'noarch'
    elif cuda:   return 'cuda'
    else:        return 'binary'


def get_host_platform():
    """Figures out what platform we are running on. Returns a platform
    identifier string - one of:

      - "linux-64"      (Linux, x86_64)
      - "linux-aarch64" (Linux, aarch64)
      - "macos-64"      (macOS, x86_64)
      - "macos-M1"      (macOS, M1/M2/M3/...)

    Note that these identifiers are for FSL releases, and are not the
    same as the platform identifiers used by conda.
    """

    platforms = {
        ('linux',  'x86_64')  : 'linux-64',
        ('linux',  'aarch64') : 'linux-aarch64',
        ('darwin', 'x86_64')  : 'macos-64',
        ('darwin', 'arm64')   : 'macos-M1',
    }

    system = platform.system().lower()
    cpu    = platform.machine()
    key    = (system, cpu)

    if key not in platforms:
        supported = ', '.join([f'[{s}, {c}]' for s, c in platforms])
        raise Exception(f'This platform [{system}, {cpu}] is unrecognised or '
                        f'unsupported! Supported platforms: {supported}')

    return platforms[key]


def get_job_platform(jobname : str) -> Tuple[str, str]:
    """Returns the platform that this CI job is running for - one of
      - noarch
      - linux-64
      - linux-aarch64
      - macos-64
      - macos-M1
      - linux-64-cuda-9.2
      - etc
    """
    # job name is assumed to have the form "<build|test>-*-conda-package",
    # e.g.:
    #  - build-linux-64-conda-package
    #  - build-linux-aarch64-conda-package
    #  - build-macos-64-conda-package
    #  - build-macos-M1-conda-package
    #  - build-noarch-conda-package
    #  - build-noarch-conda-package-direct-deploy
    #  - build-linux-cuda-9.2-conda-package
    #  - build-linux-cuda-10.2-conda-package
    #  - test-linux-64-conda-package
    pat  = r'^(build|test)-(.+)-conda-package(-direct-deploy)?$'
    arch = re.fullmatch(pat, jobname)
    if arch is None:
        return None, None

    jobtype = arch.group(1)
    arch    = arch.group(2)

    return arch, jobtype


def get_platform_shortcut_if_not_applicable(
        meta            : dict,
        pkgname         : str,
        jobname         : str,
        skip_platforms  : List[str],
        build_platforms : List[str]) -> str:
    """The FSL ci conda rules are organised such that jobs for every supported
    architecture (linux, macOS, noarch, linux+cuda-9.2, etc) is started for
    every package build. This function checks the package type, and the job
    platform, and exits early if the job platform is not applicable for the
    package type.

    If the job is not short-cutted, the platform is returned - one of:
      - noarch
      - linux-64
      - linux-aarch64
      - macos-64
      - macos-M1
      - linux-64-cuda-9.2
      - etc
    """

    pkgtype           = get_recipe_platform(meta)
    fullarch, jobtype = get_job_platform(jobname)

    if fullarch is None:
        print(f'Unable to identify architecture from '
              f'job name: {jobname}! Aborting build.')
        sys.exit(1)

    # ignore CUDA version
    arches = [fullarch]
    cuda   = re.fullmatch(r'(.*-cuda)-[0-9]+\.[0-9]+', fullarch)
    if cuda is not None:
        arches.append(cuda.group(1))

    if any(arch in skip_platforms for arch in arches):
        print(f'{fullarch} is in FSLCONDA_SKIP_PLATFORMS - '
              'aborting {jobtype}.')
        sys.exit(0)

    if not any(arch in build_platforms for arch in arches):
        print(f'{fullarch} is not in FSLCONDA_BUILD_PLATFORMS - '
              'aborting {jobtype}.')
        sys.exit(0)

    def shortcut():
        print(f'{pkgname} is a {pkgtype} package, and does not need to '
              f'be built/tested for {fullarch} (job: {jobname}). Aborting '
              f'{jobtype}.')
        sys.exit(0)

    # Only build noarch packages on noarch jobs,
    # cuda packages on cuda jobs, and binary
    # packages on linux/macos jobs.
    if (pkgtype == 'noarch') and (fullarch != 'noarch'):
        shortcut()
    if (pkgtype != 'noarch') and (fullarch == 'noarch'):
        shortcut()
    if (pkgtype == 'cuda')   and ('cuda' not in fullarch):
        shortcut()
    if (pkgtype != 'cuda')   and ('cuda' in fullarch):
        shortcut()

    return fullarch


def get_skip_platform(meta : dict) -> str:
    """Generates a value for the FSLCONDA_SKIP_PLATFORM variable. """
    pkgtype  = get_recipe_platform(meta)
    binaries = ' '.join(COMPILATION_TARGETS)
    cuda     = 'linux-64-cuda'

    if   pkgtype == 'noarch': return ' '.join((binaries, cuda))
    elif pkgtype == 'binary': return ' '.join(('noarch', cuda))
    elif pkgtype == 'cuda':   return ' '.join(('noarch', binaries))

    raise ValueError(f'Unknown platform {pkgtype}')


def get_build_platform(meta : dict) -> str:
    """Generates a value for the FSLCONDA_BUILD_PLATFORM variable. """
    pkgtype  = get_recipe_platform(meta)
    binaries = ' '.join(COMPILATION_TARGETS)
    cuda     = 'linux-64-cuda'

    if   pkgtype == 'noarch': return 'noarch'
    elif pkgtype == 'binary': return binaries
    elif pkgtype == 'cuda':   return cuda

    raise ValueError(f'Unknown platform {pkgtype}')


def get_platform_ids(meta : dict) -> List[str]:
    """Returns a sequence of platform IDs (e.g. ``'noarch'``, ``linux-64``,
    etc) for the given recipe.
    """
    pkgtype  = get_recipe_platform(meta)
    binaries = list(COMPILATION_TARGETS)
    cudas    = [f'linux-64-cuda-{ver}' for ver in CUDA_VERSIONS]

    if   pkgtype == 'noarch': return ['noarch']
    elif pkgtype == 'binary': return binaries
    elif pkgtype == 'cuda':   return cudas

    raise ValueError(f'Unknown platform {pkgtype}')
