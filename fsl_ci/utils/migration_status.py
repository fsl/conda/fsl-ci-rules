#!/usr/bin/env python
#
# Convenience script which allows you to check which version of a dependency
# packages have been built against.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import argparse
import functools as ft
import operator

import sys

import fsl_ci.gitlab as      gitlab
from   fsl_ci        import  tempdir, diskcache
from   fsl_ci.gitlab import  download_file
from   fsl_ci.conda  import (get_channel_packages,
                             load_meta_yaml,
                             sort_packages_by_dependence,
                             load_packages_from_recipe_repositories)

SERVER_URL = 'https://git.fmrib.ox.ac.uk'

CONDA_CHANNEL = "https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/"


def recipe_name_from_package_name(pkgname):
    """It'd be lovely if we could store the conda recipe URL in
    the package meta.yaml but alas.
    """

    if pkgname.endswith('-cuda-10.2'):
        pkgname = pkgname.removesuffix('-10.2')
    if pkgname.endswith('-cuda-11.0'):
        pkgname = pkgname.removesuffix('-11.0')
    if pkgname == 'fsl-cudabasisfield-cuda':
        pkgname = 'fsl-cudabasisfield'

    return pkgname


@diskcache
def download_meta_yaml(recipe_path, server, token):
    return download_file(recipe_path, 'meta.yaml', server, token)


def get_packages(args, migrations):
    all_pkgs = get_channel_packages(args.channel, linked=False)

    # Only check latest version
    all_pkgs = {name : pkgs[-1] for name, pkgs in all_pkgs.items()}

    # Sort by dependence, add a spacer between groups
    all_pkgs = sort_packages_by_dependence(all_pkgs)
    all_pkgs = [grp + [None] for grp in all_pkgs]
    all_pkgs = ft.reduce(operator.add, all_pkgs)

    pkgs   = []
    status = {}

    for pkg in all_pkgs:
        if pkg is None:
            pkgs.append(None)
            continue
        # only consider compiled projects
        if pkg.platforms == ['noarch']:
            continue

        pkgname      = pkg.name
        requires_pkg = 'n/a'

        for dep in pkg.dependencies:

            if any(m in dep.split() for m in migrations):
                requires_pkg = dep
                break

        pkgs.append(pkg)
        status[pkgname] = requires_pkg

    for p in reversed(pkgs):
        if p is None:
            pkgs.pop(-1)
        else:
            break

    return pkgs, status


def get_recipe_status(args, packages, migrations):

    results  = {}
    versions = {}

    for pkg in packages:
        if pkg is None:
            continue
        recname     = recipe_name_from_package_name(pkg.name)
        recipe_path = f'fsl/conda/{recname}'
        metayaml    = download_meta_yaml(recipe_path, args.server, args.token)

        meta  = load_meta_yaml(metayaml)
        match = 'n/a'

        for sect in meta.get('requirements', {}):
            for dep in meta['requirements'].get(sect, []):
                if any(m in dep.split() for m in migrations):
                    match = dep
                    break

        results[ pkg.name] = match
        versions[pkg.name] = meta['package']['version']
    return results, versions


def parse_args(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    usage = 'migration_status package [package ...] pin'

    parser = argparse.ArgumentParser(usage=usage)
    parser.add_argument('migration', help='Migration package name', nargs='+')
    parser.add_argument('-c', '--channel', help='Conda channel URL',
                        default=CONDA_CHANNEL)
    parser.add_argument('-s', '--server', default=SERVER_URL,
                        help=f'Gitlab server (default: {SERVER_URL})')
    parser.add_argument('-t', '--token', required=True,
                        help='Gitlab API access token with read+write access')


    args = parser.parse_args(argv)

    if len(args.migration) < 1:
        parser.error('You must provide a migration')

    return args


def main():

    gitlab.VERBOSE = False

    args       = parse_args()
    migrations = args.migration

    pkgs, pkg_status     = get_packages(args, migrations)
    rec_status, rec_vers = get_recipe_status(args, pkgs, migrations)

    print(f'Status of packages w.r.t. to [{", ".join(migrations)}]')

    names    = ['Package']
    recvers  = ['Recipe version']
    pkgvers  = ['Version']
    recstats = ['Recipe pin']
    pkgstats = ['Package pin']

    for pkg in pkgs:
        if pkg is not None:
            names   .append(pkg.name)
            recvers .append(rec_vers[pkg.name])
            pkgvers .append(pkg.version)
            recstats.append(rec_status[pkg.name])
            pkgstats.append(pkg_status[pkg.name])
        else:
            names   .append('')
            recvers .append('')
            pkgvers .append('')
            recstats.append('')
            pkgstats.append('')


    namelen    = max(len(n) for n in names)
    recverlen  = max(len(v) for v in recvers)
    pkgverlen  = max(len(v) for v in pkgvers)
    recstatlen = max(len(s) for s in recstats)
    pkgstatlen = max(len(s) for s in pkgstats)

    namefmt    = f'{{:{namelen}s}}'
    recverfmt  = f'{{:{recverlen}s}}'
    pkgverfmt  = f'{{:{pkgverlen}s}}'
    recstatfmt = f'{{:{recstatlen}s}}'
    pkgstatfmt = f'{{:{pkgstatlen}s}}'

    for name, recver, pkgver, recstat, pkgstat in zip(
            names, recvers, pkgvers, recstats, pkgstats):
        print('{} | {} | {} | {} | {}'.format(
            namefmt.format(name),
            recverfmt.format(recver),
            pkgverfmt.format(pkgver),
            recstatfmt.format(recstat),
            pkgstatfmt.format(pkgstat)))



if __name__ == '__main__':
    main()
