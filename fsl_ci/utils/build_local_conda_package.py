#!/usr/bin/env python


import             argparse
import             os
import os.path  as op
import             shutil
import             sys
import textwrap as tw


from fsl_ci                             import (sprun,
                                                indir)
from fsl_ci.gitlab                      import (gen_repository_url,
                                                get_default_branch)
from fsl_ci.platform                    import  get_host_platform
from fsl_ci.scripts.build_conda_package import  main as build_conda_package


SERVER_URL = 'https://git.fmrib.ox.ac.uk'
RECIPE_NAMESPACE = "fsl/conda"
PUB_CHANNEL = 'https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public'
DEV_CHANNEL = 'https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/development'
INT_CHANNEL = 'https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/internal'


def parse_args():
    name   = op.basename(__file__)
    usage  = f'Usage: {name} -t <token> [options] recipe [build_dir]'
    desc   = tw.dedent("""
    Build a FSL conda package locally
    """).strip()

    helps = {
        'token'      : 'Gitlab API access token with read+write access',
        'server'     : f'Gitlab server (default: {SERVER_URL})',
        'namespace'  : f'Gitlab namespace (default: {RECIPE_NAMESPACE})',
        'devrelease' : 'Build development version '
                       '(default: build stable version)',
        'ref'        : 'Recipe repository git ref to build. Ignored if '
                       'building from a local recipe directory.',
        'variable'   : 'Additional CI / CD variable to set for the build/deploy jobs',
        'recipe'     : 'FSL conda recipe repository name, or path to local '
                       'recipe directory',
        'build_dir'  : 'Local directory to clone recipe repository to. '
                       'Defaults to the recipe repository name. '
                       'Ignored if building from a local recipe directory.'
    }

    parser = argparse.ArgumentParser(usage=usage, description=desc)
    parser.add_argument('-t', '--token', help=helps['token'])
    parser.add_argument('-s', '--server', default=SERVER_URL,
                        help=helps['server'])
    parser.add_argument('-n', '--namespace', default=RECIPE_NAMESPACE,
                        help=helps['namespace'])
    parser.add_argument('-d', '--devrelease', action='store_true',
                        help=helps['devrelease'])
    parser.add_argument('-r', '--ref', help=helps['ref'])
    parser.add_argument('-v', '--variable', nargs=2, metavar=('NAME', 'VALUE'),
                        action='append', help=helps['variable'])
    parser.add_argument('recipe', help=helps['recipe'])
    parser.add_argument('build_dir', help=helps['build_dir'], nargs='?')

    args = parser.parse_args()

    if args.token is None:
        args.token = os.environ['FSL_CI_API_TOKEN']

    if op.exists(args.recipe):
        args.recipe     = op.abspath(args.recipe)
        args.build_dir = args.recipe
        args.ref        = args.recipe
    else:
        if args.build_dir is None:
            args.build_dir = args.recipe
        args.build_dir = op.abspath(args.build_dir)
        args.recipe    = f'{args.namespace}/{args.recipe}'

    if args.variable is None: args.variable = {}
    else:                     args.variable = dict(args.variable)
    return args


def checkout_recipe(args):
    recipe_url = gen_repository_url(args.recipe, args.server, args.token)
    if args.ref is None:
        args.ref = get_default_branch(args.recipe, args.server, args.token)
    sprun(f'git clone {recipe_url} {args.build_dir}')
    with indir(args.build_dir):
        sprun(f'git checkout {args.ref}')


def do_build(args):
    # CI_PROJECT_URL/COMMIT_REF_NAME are only used for logging
    jobname = f'build-{get_host_platform()}-conda-package'
    os.environ['CI_PROJECT_URL']                   = args.recipe
    os.environ['CI_COMMIT_REF_NAME']               = args.ref
    os.environ['CI_JOB_NAME']                      = jobname
    os.environ['CI_SERVER_URL']                    = args.server
    os.environ['FSL_CI_API_TOKEN']                 = args.token
    os.environ['FSLCONDA_PUBLIC_CHANNEL_URL']      = PUB_CHANNEL
    os.environ['FSLCONDA_INTERNAL_CHANNEL_URL']    = INT_CHANNEL
    os.environ['FSLCONDA_DEVELOPMENT_CHANNEL_URL'] = DEV_CHANNEL

    for k, v in args.variable.items():
        os.environ[k] = v

    if args.devrelease:
        os.environ['DEVRELEASE'] = 'true'

    with indir(args.build_dir):
        build_conda_package()


def main():
    args = parse_args()
    if op.exists(args.recipe): ref = args.recipe
    else:                      ref = checkout_recipe(args)
    do_build(args)


if __name__ == '__main__':
    sys.exit(main())
