#!/usr/bin/env python
#
# Check out a repository which has files stored with git lfs.
#
# We use $GIT_CONFIG in the fsl-ci-rules to isolate the git
# configuration used by a job from the system/user git
# configuration (as we use runners on physical shared
# machines).
#
# However, the git-lfs command does not honour git configuration
# file settings, e.g. $GIT_CONFIG, or the --file option. This
# results in conflicts and errors when working with git-lfs-enabled
# repositories.
#
# So this script performs a convoluted series of steps to
# successfully clone a git-lfs repository, with all files checked
# out.
#

import            os
import os.path as op
import            sys
import            glob
import            shutil
import            zipfile

from fsl_ci import sprun, gitlab, tempdir


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]
    if len(argv) not in (2, 3):
        print('usage: git_lfs_clone project destination_directory [rev]')
        sys.exit(1)

    token  = os.environ['FSL_CI_API_TOKEN']
    server = os.environ.get('CI_SERVER_URL', 'https://git.fmrib.ox.ac.uk')

    project  =            argv[0]
    destdir  = op.abspath(argv[1])

    if len(argv) == 3: rev = argv[2]
    else:              rev = None

    with tempdir() as td:

        # A while back I gave up on using git lfs to check out
        # a repository, and resorted to downloading a .zip
        # archive from gitlab. However, gitlab archives downloaded
        # from the non-default branch of a project don't contain
        # LFS files :(
        #
        # https://gitlab.com/gitlab-org/gitlab/-/issues/15079
        data = gitlab.download_archive(project, server, token, rev)

        with open('file.zip', 'wb') as f:
            f.write(data)

        zipfile.ZipFile('file.zip').extractall('.')

        dirname = [f for f in glob.glob('./*') if not f.endswith('file.zip')][0]

        shutil.move(dirname, destdir)

        # So here I scan every file in the repository to see if it
        # is a LFS file, and do an additional download for each
        lfsfiles = {}
        for dirpath, _, filenames in os.walk(destdir):
            for filename in filenames:
                fullfile = op.join(dirpath, filename)
                relfile  = fullfile.removeprefix(destdir).lstrip('/')

                with open(fullfile, 'rb') as f:
                    hdr = f.read(35)
                    if hdr == b'version https://git-lfs.github.com/':
                        lfsfiles[relfile] = fullfile

        for src, dest in lfsfiles.items():
            content = gitlab.download_file(project, src, server, token, ref=rev, text=False)
            with open(dest, 'wb') as f:
                f.write(content)

if __name__ == '__main__':
    sys.exit(main())
