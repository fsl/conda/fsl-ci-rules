#!/usr/bin/env python
#
# apply_patch.py - Apply a patch to a conda recipe
#
# This script is intended to be called manually to automatically
# update a FSL conda recipe repository.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import glob
import os.path as op
import os
import time
import sys
import argparse
import textwrap as tw

from fsl_ci        import (tempdir,
                           sprun,
                           spcap,
                           indir)
from fsl_ci.gitlab import (open_merge_request,
                           merge_merge_request,
                           gen_branch_name,
                           get_default_branch,
                           gen_repository_url,
                           list_pipelines,
                           cancel_pipeline)
from fsl_ci.recipe import (get_recipe_variable,
                           update_recipe_variable)


RECIPE_NAMESPACE = "fsl/conda"
"""Default recipe namespace, if not specified on the command.line."""

SERVER_URL = 'https://git.fmrib.ox.ac.uk'
"""Default gitlab instance URL, if not specified on the command.line."""


def apply_patch(patch, is_patch_file):
    # assumes recipe dir is cwd

    with open('meta.yaml', 'rt') as f:
        metayaml = f.read()

    oldbld   = get_recipe_variable(metayaml, 'build')
    newbld   = int(oldbld) + 1
    metayaml = update_recipe_variable(metayaml, 'build', newbld)

    if not is_patch_file:
        old, new = patch.split('|||')

        if old not in metayaml:
            print(f'Old text "{old}" is not present in meta.yaml! Skipping')
            return False

        metayaml = metayaml.replace(old, new)

    with open('meta.yaml', 'wt') as f:
        f.write(metayaml)

    if is_patch_file:
        with open(patch, 'rt') as f:
            sprun('patch -p1', stdin=f)

    backups = glob.glob('*.orig')
    if len(backups) > 0:
        sprun(f'rm -f {" ".join(backups)}')

    return True


def checkout_and_patch_recipe(recipe_path, patch, is_patch_file,
                              commitmsg, server, token):

    recipe_url = gen_repository_url(recipe_path, server, token)
    defbranch  = get_default_branch(recipe_path, server, token)
    branch     = gen_branch_name('mnt/patch',
                                 recipe_path,
                                 server,
                                 token)

    if commitmsg is None:
        commitmsg = tw.dedent(f"""
        MNT: Update {recipe_path} (apply_patch.py)
        """).strip()

    with tempdir():
        sprun(f'git clone {recipe_url} recipe')
        with indir('recipe'):
            sprun(f'git checkout -b {branch} {defbranch}')

            if not apply_patch(patch, is_patch_file):
                return None

            sprun( 'git add *')
            sprun(f'git commit -m "{commitmsg}"')
            sprun(f'git push origin {branch}')

            diff = spcap(f'git diff {defbranch} {branch}', verbose=False)
            print(f'\n\nUpdated {recipe_path}:\n')
            print(f'{diff}\n\n')

    return branch


def cancel_latest_pipeline(project_path, server, token, ref=None):
    if ref is None:
        ref  = get_default_branch(project_path, server, token)
    pipes  = list_pipelines(project_path, server, token, ref=ref)
    pipeid = pipes[0]['id']
    if pipeid is not None:
        cancel_pipeline(project_path, pipeid, server, token)


def parse_args():
    epilog = tw.dedent("""
    You can provide either a patch file (e.g. produced by "git diff", or a
    replacement string of the form "old text|||new text", which will be
    applied to the recipe meta.yaml file.

    You can either list all recipes on the command-line, or pass a text
    file with one recipe per line.
    """)

    parser = argparse.ArgumentParser(op.basename(__file__), epilog=epilog)

    parser.add_argument('patch',  help='patch file or replacement string')
    parser.add_argument('recipe',
                        help='recipe  or text file with recipes', nargs='+')
    parser.add_argument('-s', '--server', help='gitlab server URL',
                        default=SERVER_URL)
    parser.add_argument('-t', '--token', help='gitlab API token')
    parser.add_argument('-n', '--namespace', default=RECIPE_NAMESPACE,
                        help=f'GItlab namespace (default: {RECIPE_NAMESPACE})')
    parser.add_argument('-m', '--message', help='Commit message')
    parser.add_argument('-r', '--mr_message', help='Merge request message')
    parser.add_argument('-i', '--mr_title', help='Merge request title')
    parser.add_argument('-a', '--automerge', help='Automatically merge',
                        action='store_true')
    parser.add_argument('-c', '--cancel_pipeline',
                        help='Cancel build pipeline(s)',
                        action='store_true')

    args = parser.parse_args()

    args.is_patch_file = op.exists(args.patch)

    if args.mr_message is None:
        args.mr_message = args.message

    if args.is_patch_file:
        with open(args.patch, 'rt') as f:
            args.patch_contents = f.read()
    else:
        args.patch_contents = args.patch

    if (len(args.recipe) == 1) and op.exists(args.recipe[0]):
        recipes     = open(args.recipe[0]).readlines()
        recipes     = [r.strip() for r in recipes]
        recipes     = [r for r in recipes if r != '']
        args.recipe = recipes

    if len(args.recipe) > 0:
        args.recipe = [f'{args.namespace}/{r}' for r in args.recipe]

    if args.token is None:
        args.token = os.environ['FSL_CI_API_TOKEN']

    return args


def main():

    args = parse_args()

    mrmsg = tw.dedent("""
    This merge request was triggered by the fsl-ci-rules
    `apply_patch.py` script. The patch that has been applied is as follows:
    ```
    """).strip()

    mrmsg += "\n" + args.patch_contents
    mrmsg += "\n```\n"

    if args.mr_message is not None:
        mrmsg += f'\n{args.mr_message}\n'

    mrtitle = "[fsl_ci: apply_patch]"

    if args.mr_title is not None:
        mrtitle  = f'{mrtitle}: {args.mr_title}'

    for recipe in args.recipe:
        branch = checkout_and_patch_recipe(recipe,
                                           args.patch,
                                           args.is_patch_file,
                                           args.message,
                                           args.server,
                                           args.token)

        if branch is None:
            print(f'Patch failed - skipping {recipe}')
            continue

        mr_result = open_merge_request(recipe,
                                       branch,
                                       mrmsg,
                                       args.server,
                                       args.token,
                                       title=mrtitle)

        print(f'\n\nMerge request opened on {recipe}: {mr_result["web_url"]}')

        # Cancel the pipeline that was created
        # from the merge if there was one.
        if args.cancel_pipeline:
            print('\n\nCancelling MR pipeline ...')
            time.sleep(3)
            cancel_latest_pipeline(recipe, args.server, args.token, branch)

        if args.automerge:
            # Give gitlab a few seconds between
            # operations, and try a few times,
            # as gitlab often emits HTTP 422
            # Unprocessable entity errors.
            for i in range(3):
                print('\n\nAuto-merging MR ...')
                time.sleep(5)
                try:
                    merge_merge_request(recipe, mr_result['iid'],
                                        args.server, args.token)
                    break
                except Exception as e:
                    if i == 2:
                        raise e

            if args.cancel_pipeline:
                print('\n\nCancelling merged pipeline ...')
                time.sleep(3)
                cancel_latest_pipeline(recipe, args.server, args.token)



if __name__ == '__main__':
    sys.exit(main())
