#!/usr/bin/env python
#
# Trigger a CI pipeline, or manual job, on a gitlab project repository from
# the command-line.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import os.path  as op
import textwrap as tw
import             sys
import             argparse

from fsl_ci.gitlab import (trigger_job,
                           trigger_pipeline,
                           get_default_branch,
                           find_latest_job)


SERVER_URL = 'https://git.fmrib.ox.ac.uk'
"""Default gitlab instance URL, if not specified on the command.line."""


def parseArgs():
    name   = op.basename(__file__)
    usage  = f'Usage: {name} -t <token> [options] project_path'
    desc  = tw.dedent("""
    This script can be used to trigger a pipeline, or a specific pipeline
    job, on a gitlab repository.
    """).strip()

    helps  = {
        'project_path' :
        'Path (namespace/project) of the gitlab project repository',

        'token' :
        'Gitlab API access token with read+write access',

        'server' :
        f'Gitlab server (default: {SERVER_URL})',

        'ref' :
        'Git ref to run pipeline on (default: master).',

        'job' :
        'If provided, instead of a pipeline being triggered on a ref, '
        'the most recently submitted job with this name is triggered.',

        'age' :
        'Only consider jobs which are at most this old, in hours '
        '(default: 1). Only used if --job is specified.',
    }

    parser = argparse.ArgumentParser(
        usage=usage,
        description=desc,
        formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('project_path',   help=helps['project_path'])
    parser.add_argument('-s', '--server', help=helps['server'],
                        default=SERVER_URL)
    parser.add_argument('-t', '--token',  help=helps['token'],
                        required=True)
    parser.add_argument('-a', '--age',    help=helps['age'],
                        type=int, default=1)
    grp = parser.add_mutually_exclusive_group()
    grp.add_argument('-r', '--ref', help=helps['ref'])
    grp.add_argument('-j', '--job', help=helps['job'])

    args = parser.parse_args()

    if args.ref is None:
        args.ref = get_default_branch(args.project_path,
                                      args.server,
                                      args.token)

    return args


def trigger_specified_job(args):
    """Triggers the job specified  by --job."""

    job  = find_latest_job(args.project_path,
                           args.server,
                           args.token,
                           jobpat=args.job,
                           age=args.age)[0]

    if job is None:
        print(f'Cannot find a CI job on project {args.project_path} '
              f'with name {args.job} - aborting.')
        sys.exit(1)

    trigger_job(args.project_path, job['id'], args.server, args.token)


def trigger_specified_pipeline(args):
    """Triggers a full pipieline on --ref (default: master)."""

    trigger_pipeline(args.project_path,
                     args.ref,
                     args.server,
                     args.token)


def main():
    args = parseArgs()
    if args.job is not None: trigger_specified_job(     args)
    else:                    trigger_specified_pipeline(args)


if __name__ == '__main__':
    main()
