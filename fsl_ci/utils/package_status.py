#!/usr/bin/env python
#
# Convenience script which checks the package status of all FSL conda recipes.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#
"""This script generates a table containing information about every conda
recipe in the gitlab fsl/conda/ space. The table contains columns for:
 - The recipe package name
 - The corresponding project repository
 - The latest tag on the project repository
 - The version listed in the recipe meta.yaml file
 - The latest package version in the internal FSL channel
 - The latest package version in the development FSL channel
 - The latest package version in the public FSL channel
"""


import                   sys
import                   argparse
import itertools      as it
import urllib.parse   as urlparse
import urllib.request as urlrequest

from fsl_ci.gitlab import (get_projects_in_namespace,
                           download_file,
                           lookup_project_tags)
from fsl_ci.recipe import  get_recipe_variable
from fsl_ci.conda  import (load_meta_yaml,
                           get_channel_packages,
                           load_packages_from_recipe_repositories,
                           sort_packages_by_dependence)


SERVER_URL = 'https://git.fmrib.ox.ac.uk'
"""Default gitlab instance URL, if not specified on the command.line."""


RECIPE_NAMESPACE = "fsl/conda"
"""Default recipe namespace, if not specified on the command.line."""


INTERNAL_CHANNEL_URL = "https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/internal/"
"""Default internal channel URL, if not specified on the command.line."""


DEVELOPMENT_CHANNEL_URL = "https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/development/"
"""Default development channel URL, if not specified on the command.line."""


PUBLIC_CHANNEL_URL = "https://fsl.fmrib.ox.ac.uk/fsldownloads/fslconda/public/"
"""Default public channel URL, if not specified on the command.line."""


PLATFORMS = ['noarch', 'linux-64', 'linux-aarch64', 'osx-64', 'osx-arm64']
"""Default platforms to check package availability for. """


CUDA_VERS = ['10.2']
"""Assume that CUDA packatges are available for these CUDA versions."""


MISSING = '-'
"""Value to use in output table for missing/irrelevant cells. """


PLATFORM_CODES = {
    'noarch'         : 'N',
    'linux-64'       : 'L',
    'linux-aarch64'  : 'A',
    'osx-64'         : 'O',
    'osx-arm64'      : 'M'
}
"""Identifiers for all platforms, used in printed table."""


EXCLUDE = ['docs', 'installer', 'manifest', 'manifest-rules', 'fsl-ci-rules']
"""List of repositories in the fsl/conda/ gitlab namespace to ignore. """


def get_recipe_info(project_path, server, token):
    """Return the name, version, and project repository of the given conda
    recipe repository.
    """
    def getval(d, *keys, default=None):
        for k in keys:
            d = d.get(k, None)
            if d is None:
                break

        if d is None: return default
        else:         return d

    raw         = download_file(project_path, 'meta.yaml', server, token)
    metayaml    = load_meta_yaml(raw)
    name        = getval(metayaml, 'package', 'name')
    version     = getval(metayaml, 'package', 'version')
    repository  = getval(metayaml, 'source',  'git_url')
    noarch      = getval(metayaml, 'build',   'noarch')

    if name       is None: name       = get_recipe_variable(raw, 'name')
    if version    is None: version    = get_recipe_variable(raw, 'version')
    if repository is None: repository = get_recipe_variable(raw, 'repository')
    if name       is None: name       = MISSING
    if version    is None: version    = MISSING
    if repository is None: repository = MISSING

    if noarch in ('generic', 'python'):
        arch = noarch
    else:
        reqs = getval(metayaml, 'requirements', 'host', default=[])
        if 'compiler' in reqs:
            arch = 'native'
        else:
            arch = 'generic (needs update)'

    return {
        'name'       : name,
        'version'    : version,
        'repository' : repository,
        'arch'       : arch,
    }


def get_project_info(project_repo, server, token):
    """Return the project name, and most recent tag, for the given
    project repository.
    """

    name = MISSING
    tag  = MISSING

    if project_repo is not None:
        try:
            project_path = urlparse.urlparse(project_repo)[2][1:]
            if project_path.endswith('.git'):
                project_path = project_path[:-4]
            name         = project_path.split('/')[-1]
            tags         = lookup_project_tags(project_path, server, token)

            if len(tags) > 0: tag = tags[0]
            else:             tag = MISSING
        # the project repo is probably not on gitlab (e.g. on github)
        except Exception:
            pass

    return {
        'repository' : project_repo,
        'path'       : project_path,
        'name'       : name,
        'tag'        : tag
    }


def parseArgs():

    helps = {
        'token' :
        'Gitlab API access token with read+write access',

        'username' : 'Username to access internal conda channel',
        'password' : 'Password to access internal conda channel',

        'server' :
        f'Gitlab server (default: {SERVER_URL})',

        'namespace' :
        'Gitlab namespace which contains the conda '
        f'recipes (default: {RECIPE_NAMESPACE}',

        'internal_url'    : 'URL of the internal conda channel',
        'public_url'      : 'URL of the public conda channel',
        'development_url' : 'URL of the development conda channel',

        'output' :
        'Save to this output file, instead of printing to standard output',

        'exclude' :
        'Exclude packages with this repository name. Can be used '
        f'multiple times. These projects are always excluded: {EXCLUDE}',

        'platform' :
        'Check package availability for these platforms (default: noarch, '
        'linux-64, linux-aarch64, osx-64). Can be used multiple times'
    }

    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--server', default=SERVER_URL,
                        help=helps['server'])
    parser.add_argument('-t', '--token', required=True,
                        help=helps['token'])
    parser.add_argument('-u', '--username', help=helps['username'])
    parser.add_argument('-p', '--password', help=helps['password'])
    parser.add_argument('-n', '--namespace',
                        help=helps['namespace'],
                        default=RECIPE_NAMESPACE)
    parser.add_argument('-iu', '--internal_url',
                        help=helps['internal_url'],
                        default=INTERNAL_CHANNEL_URL)
    parser.add_argument('-pu', '--public_url',
                        help=helps['public_url'],
                        default=PUBLIC_CHANNEL_URL)
    parser.add_argument('-du', '--development_url',
                        help=helps['development_url'],
                        default=DEVELOPMENT_CHANNEL_URL)
    parser.add_argument('-o', '--output',
                        help=helps['output'])
    parser.add_argument('-e', '--exclude',
                        help=helps['exclude'], action='append')
    parser.add_argument('-pl', '--platform',
                        help=helps['platform'],
                        default=PLATFORMS,
                        action='append')

    args = parser.parse_args()

    if args.exclude is None:
        args.exclude = []

    args.exclude.extend(EXCLUDE)

    if (args.username is not None) and (args.password is     None) or \
       (args.username is     None) and (args.password is not None):
        parser.error('Both --username and --password must be specified')

    # install auth handler to
    # access internal channel
    if args.username is not None:
        pwdmgr = urlrequest.HTTPPasswordMgrWithDefaultRealm()
        pwdmgr.add_password(
            None, args.internal_url, args.username, args.password)
        handler = urlrequest.HTTPBasicAuthHandler(pwdmgr)
        opener  = urlrequest.build_opener(handler)
        opener.open(args.internal_url)
        urlrequest.install_opener(opener)

    return args


def print_columns(titles, columns, f=None):
    """Convenience function which pretty-prints a collection of columns in a
    tabular format.

    :arg titles:  A list of titles, one for each column.

    :arg columns: A list of columns, where each column is a list of strings.
    """

    if f is None:
        f = sys.stdout

    cols  = []

    for t, c in zip(titles, columns):
        cols.append([t] + list(map(str, c)))

    columns = cols
    colLens = []

    for col in columns:
        maxLen = max([len(r) for r in col])
        colLens.append(maxLen)

    fmtStr = ' | '.join(['{{:<{}s}}'.format(l) for l in colLens]) + '\n'

    titles  = [col[0]  for col in columns]
    columns = [col[1:] for col in columns]

    separator = ['-' * l for l in colLens]

    f.write(fmtStr.format(*titles))
    f.write(fmtStr.format(*separator))

    nrows = len(columns[0])
    for i in range(nrows):

        row = [col[i] for col in columns]
        f.write(fmtStr.format(*row))


def filter_projects(projects, exclude):
    """Removes projects which the user has requested be excluded.
    """

    if exclude is None:
        return projects

    filtered = []
    for project in projects:

        name = project.rsplit('/')[-1]

        if name in exclude:
            print(f'Removing {project}')
        else:
            filtered.append(project)

    return filtered


def main():
    args       = parseArgs()
    internal   = get_channel_packages(args.internal_url)
    devel      = get_channel_packages(args.development_url)
    public     = get_channel_packages(args.public_url)
    projects   = get_projects_in_namespace(args.namespace,
                                           args.server,
                                           args.token)
    projects   = filter_projects(projects, args.exclude)
    projects   = load_packages_from_recipe_repositories(projects,
                                                        args.server,
                                                        args.token)
    projects   = sort_packages_by_dependence(projects)


    # Print a table containing the following columns
    # package name, recipe version, project repo <- get_recipe_info
    # latest project tag                         <- get_project_info
    # internal, devel, public versions           <- get_channel_version
    # available bullds (e.g. linux,macos,noarch) <- get_channel_packages
    titles = ['Package', 'Repository', 'Recipe version', 'Latest tag',
              'Internal', 'Development', 'Public', 'Builds']
    packagenames = []
    recipevers   = []
    projectrepos = []
    projecttags  = []
    internalvers = []
    publicvers   = []
    develvers    = []
    builds       = []

    # add a divider between each group
    projects = list(it.chain(*[group + [None] for group in projects]))

    for project in projects:

        # divider
        if project is None:
            packagenames.append('')
            recipevers  .append('')
            projectrepos.append('')
            projecttags .append('')
            internalvers.append('')
            publicvers  .append('')
            develvers   .append('')
            builds      .append('')
            continue

        project_path = project.recipe_path

        recipe  = get_recipe_info(project_path, args.server, args.token)
        repo    = recipe['repository']
        package = recipe['name']
        project = get_project_info(repo, args.server, args.token)

        if 'CUDA_VER' in package:
            packages = [package.replace('CUDA_VER', cv) for cv in CUDA_VERS]
        else:
            packages = [package]

        for package in packages:

            if package in internal: internalver = internal[package][-1].version
            else:                   internalver = MISSING
            if package in devel:    develver    = devel[package][-1].version
            else:                   develver    = MISSING
            if package in public:   publicver   = public[package][-1].version
            else:                   publicver   = MISSING

            buildstr = ''
            for channel in [internal, devel, public]:
                for plat in args.platform:
                    if package in channel and plat in channel[package][-1].platforms:
                        buildstr += PLATFORM_CODES.get(plat, plat[-1].upper())
                    else:
                        buildstr += '-'
                buildstr += '|'
            buildstr = buildstr[:-1]

            if args.server in repo:
                repo = repo.replace(args.server, '<git@fmrib>')

            packagenames.append(package)
            recipevers  .append(recipe['version'])
            projectrepos.append(repo)
            projecttags .append(project['tag'])
            internalvers.append(internalver)
            develvers   .append(develver)
            publicvers  .append(publicver)
            builds      .append(buildstr)

    if args.output is not None:
        outf = open(args.output, 'wt')
    else:
        outf = None

    try:
        print_columns(titles,
                      (packagenames, projectrepos, recipevers,
                       projecttags, internalvers, develvers,
                       publicvers, builds),
                      outf)
    finally:
        if outf is not None:
            outf.close()


if __name__ == '__main__':
    main()
