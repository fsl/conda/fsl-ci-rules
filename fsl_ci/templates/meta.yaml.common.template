# This is a FSL conda recipe for:              {{ template_name              }}
# The project git repository is:               {{ template_repository        }}
# The git repository for this conda recipe is: {{ template_recipe_repository }}

{% if 'cuda' not in sub_template %}
{{ '{%' }} set name       = '{{ template_name       }}' {{ '%}' }}
{{ '{%' }} set version    = '{{ template_version    }}' {{ '%}' }}
{{ '{%' }} set repository = '{{ template_repository }}' {{ '%}' }}
{{ '{%' }} set build      = '{{ template_build      }}' {{ '%}' }}
{% else %}
# Note that multiple conda packages are built from
# this recipe - one package is built for each
# supported CUDA version, and named
# "<name>-cuda-X.Y", where X.Y is the CUDA version
# that the package was compiled against.

# The CUDA_VER environment variable must be set when
# a CUDA project is built. CUDA_VER should contain
# the major.minor CUDA toolkit version. CUDA_VER is
# pre-set in the conda-forge and FSL development
# CUDA build docker images, but needs to be explicitly
# set when building locally.
{{ '{%' }} set cuda_version = os.environ["CUDA_VER"]  {{ '%}' }}
{{ '{%' }} set cuda_label   = '-cuda-' + cuda_version {{ '%}' }}
{{ '{%' }} set name         = '{{ template_name       }}' + cuda_label {{ '%}' }}
{{ '{%' }} set version      = '{{ template_version    }}' {{ '%}' }}
{{ '{%' }} set repository   = '{{ template_repository }}' {{ '%}' }}
{{ '{%' }} set build        = '{{ template_build      }}' {{ '%}' }}
{% endif %}

package:
  name:    {{ '{{ name    }}' }}
  version: {{ '{{ version }}' }}

source:
  # the FSLCONDA_REPOSITORY and FSLCONDA_REVISION
  # environment variables can be used to override
  # the repository/revision for development purposes.
  git_url: {{ '{{ os.environ.get("FSLCONDA_REPOSITORY", repository) }}' }}
  git_rev: {{ '{{ os.environ.get("FSLCONDA_REVISION",   version)    }}' }}

{% include sub_template %}
{% if test %}{{ test }}{% endif %}
{% if about %}{{ about }}{% endif %}