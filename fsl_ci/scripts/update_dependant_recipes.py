#!/usr/bin/env python
#
# update_dependant_recipes.py - Updates the version of a project in dependant
#                               recipes.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import os.path  as op
import             os
import             sys
import textwrap as tw

from fsl_ci        import (USERNAME,
                           EMAIL,
                           indir,
                           tempdir,
                           sprun)
from fsl_ci.gitlab import (gen_repository_url,
                           gen_branch_name,
                           get_default_branch,
                           open_merge_request)
from fsl_ci.recipe import (update_recipe_dependency,
                           get_recipe_variable,
                           update_recipe_variable)
from fsl_ci.conda  import (load_meta_yaml,
                           get_channel_packages,
                           find_dependant_packages)


def update_recipe(recipe_dir, depname, depver):
    majorver = depver.split('.')[0]
    filename = op.join(recipe_dir, 'meta.yaml')
    with open(filename, 'rt') as f:
        meta = f.read()

    build = get_recipe_variable(meta, 'build')
    build = str(int(build) + 1)
    meta  = update_recipe_dependency(meta, depname, majorver)
    meta  = update_recipe_variable(meta, 'build', build)

    with open(filename, 'wt') as f:
        f.write(meta)


def checkout_and_update_recipe(recipe_path, depname, depver, server, token):
    recipe_url  = gen_repository_url(recipe_path, server, token)
    defbranch   = get_default_branch(recipe_path, server, token)
    branch_name = gen_branch_name('mnt/update-dependencies',
                                  recipe_path, server, token)
    commitmsg   = f'MNT: Update {depname} version number to ' \
                  f'{depver} (automatic fsl-ci-rules)'

    with tempdir():
        sprun(f'git clone {recipe_url} recipe')
        with indir('recipe'):
            sprun(f'git config user.name  {USERNAME}')
            sprun(f'git config user.email {EMAIL}')
            sprun(f'git checkout -b {branch_name} {defbranch}')
            update_recipe('.', depname, depver)
            sprun( 'git add *')
            sprun(f'git commit -m "{commitmsg}"')
            sprun(f'git push origin {branch_name}')

    return branch_name


def get_dependant_recipe_paths(pkgname, channel_url):
    pkgs       = get_channel_packages(channel_url)
    dependants = find_dependant_packages(pkgname, pkgs)
    paths      = [f'fsl/conda/{dep}' for dep in dependants]
    return paths


def main():

    this_repo = os.environ['CI_PROJECT_PATH']
    server    = os.environ['CI_SERVER_URL']
    token     = os.environ['FSL_CI_API_TOKEN']
    channel   = os.environ['FSLCONDA_PUBLIC_CHANNEL_URL']
    meta      = load_meta_yaml('meta.yaml')
    pkgname   = meta['package']['name']
    pkgver    = meta['package']['version']

    deps = os.environ.get('FSLCONDA_DEPENDANT_RECIPES', None)
    if deps is None:
        deps = get_dependant_recipe_paths(pkgname, channel)

    for dep in deps:
        branch = checkout_and_update_recipe(dep, pkgname, pkgver,
                                            server, token)
        mrmsg  = tw.dedent(f"""
        This merge request was triggered by a new version of the
        `{this_repo}` project being released.

        The `{dep}` project lists `{this_repo}` as a dependency, so
        `{dep}` might need to be re-built to ensure ABI compatibility.

        (MR automatically generated by fsl-ci-rules running on the
        `{this_repo}` repository).
        """).strip()

        open_merge_request(dep, branch, mrmsg, server, token)



if __name__ == '__main__':
    sys.exit(main())
