#!/usr/bin/env python
#
# This script is run from a CI job on the project repository. It triggers
# a pipeline on the accompanying recipe repository
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import os
import datetime

from fsl_ci.conda  import  get_recipe_urls
from fsl_ci.gitlab import (trigger_pipeline,
                           get_default_branch)


def main():

    # TODO gen timestamp here
    server       = os.environ['CI_SERVER_URL']
    token        = os.environ['FSL_CI_API_TOKEN']
    project_path = os.environ['CI_PROJECT_PATH']
    project_repo = os.environ['CI_PROJECT_URL']
    project_ref  = os.environ['CI_COMMIT_REF_NAME']
    recipes      = get_recipe_urls(project_path, server)

    # These variables are passed to the
    # build-conda-package job on the
    # recipe repositories. They control
    # the repo/revision from which the
    # package is built, and whether the
    # built package is published to the
    # public or development channel.
    #
    # The timestamp used in the dev
    # release version is generated here,
    # so that it is identical across
    # all platforms.
    timestamp = datetime.datetime.utcnow().strftime('%Y%m%d%H%M')
    variables = {
        'FSLCONDA_REVISION'    : project_ref,
        'FSLCONDA_REPOSITORY'  : project_repo,
        'DEVRELEASE'           : 'true',
        'DEVRELEASE_TIMESTAMP' : timestamp,
    }

    # start the pipeline
    for recipe in recipes:
        recipe    = recipe['path']
        defbranch = get_default_branch(recipe, server, token)
        pipeline  = trigger_pipeline(recipe, defbranch, server, token, variables)
        url       = pipeline['web_url']

        print(f'Triggered pipeline on {recipe} - see {url} for more details.')

    # We do not wait for the pipeline to finish because our
    # CI runner infrastructure can only run a limited number
    # of jobs, and having one job waiting on another uses
    # up a valuable job slot.


if __name__ == '__main__':
    main()
