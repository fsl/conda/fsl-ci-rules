#!/usr/bin/env python
#
# This script is executed when a new tag is added to a FSL project
# repository. It updates the conda recipe for the project, using the new tag
# as the version, and then opens a merge request on the recipe repository to
# update the recipe.
#
# Thanks to: https://gitlab.com/tmaier/gitlab-auto-merge-request
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import os.path  as op
import             os
import             sys
import textwrap as tw

from fsl_ci            import (USERNAME,
                               EMAIL,
                               tempdir,
                               indir,
                               sprun)
from fsl_ci.versioning import  is_valid_project_version
from fsl_ci.conda      import (get_recipe_urls,
                               get_channel_packages,
                               find_dependant_packages)
from fsl_ci.gitlab     import (http_request,
                               lookup_project_id,
                               open_merge_request,
                               gen_branch_name,
                               get_default_branch,
                               gen_repository_url)
from fsl_ci.fsl        import (get_fsl_project_type,
                               is_tcl_project,
                               get_project_executables,
                               get_python_executables)
from fsl_ci.recipe     import  update_recipe_variable


def check_mr_already_open(
        server, token, project_id, source_branch, target_branch):
    """Checks to see if a MR is already open on the specified project, from
    the source to target
    """

    url      = f'{server}/api/v4/merge_requests?state=opened'
    response = http_request(url, token)

    for mr in response:
        if all((mr['source_project_id'] == project_id,
                mr['target_project_id'] == project_id,
                mr['source_branch']     == source_branch,
                mr['target_branch']     == target_branch)):
            return True

    return False


def update_recipe(project_dir, recipe_dir, newver, newbld):
    """Updates the version and build numbers in the recipe meta.yaml.
    Assumes that the version and build numbers are defined like so:

        {% set version = 'version' %}
        {% set build   = 'build'   %}

    If this is not the case, the version/build strings will not be updated.
    See the recipe.update_recipe_variable function for more details.
    """

    filename = op.join(recipe_dir, 'meta.yaml')
    with open(filename) as f:
        metayaml = f.read()

    metayaml = update_recipe_variable(metayaml, 'version', newver)
    metayaml = update_recipe_variable(metayaml, 'build',   newbld)

    with open(filename, 'wt') as f:
        f.write(metayaml)


def checkout_and_update_recipe(recipe,
                               defbranch,
                               branch,
                               project_path,
                               project_version,
                               build):

    project_dir = os.getcwd()
    recipe_url  = recipe['url']

    commitmsg = tw.dedent(f"""
    MNT: Update {project_path} version number
    to {project_version} [build {build}] (automatic fsl-ci-rules)
    """).strip()

    with tempdir():
        sprun(f'git clone {recipe_url} recipe')
        with indir('recipe'):
            sprun(f'git config user.name  {USERNAME}')
            sprun(f'git config user.email {EMAIL}')
            sprun(f'git checkout -b {branch} {defbranch}')
            update_recipe(project_dir,
                          os.getcwd(),
                          project_version,
                          build)
            sprun( 'git add *')
            sprun(f'git commit -m "{commitmsg}"')
            sprun(f'git push origin {branch}')


def main(server=None,
         token=None,
         project_path=None,
         project_name=None,
         project_ver=None,
         build_num=0,
         mrmsg=None,
         recipe_path=None,
         return_mrurl=False):
    """Re-generates the conda recipe(s) associated with a FSL project.  In
    normal execution, all of the arguments are read from the environment.

    :arg server:       GitLab server url
    :arg token:        GitLab API access token
    :arg project_path: GitLab project path
    :arg project_name: Project name
    :arg project_ver:  New project tag
    :arg build_num:    Build number
    :arg mrmsg:        Message to use for the merge request
    :arg recipe_path:  Specifically rerender this recipe
    """

    if server       is None: server       = os.environ['CI_SERVER_URL']
    if token        is None: token        = os.environ['FSL_CI_API_TOKEN']
    if project_path is None: project_path = os.environ['CI_PROJECT_PATH']
    if project_name is None: project_name = os.environ['CI_PROJECT_NAME']
    if project_ver  is None: project_ver  = os.environ['CI_COMMIT_REF_NAME']

    conda_channel = os.environ['FSLCONDA_PUBLIC_CHANNEL_URL']

    skip = os.environ.get('FSLCONDA_SKIP_RECIPE_UPDATE', None)

    if recipe_path is not None:
        recipes = [{'url'  : gen_repository_url(recipe_path, server, token),
                    'name' : recipe_path.split('/')[-1],
                    'path' : recipe_path}]
    else:
        recipes = get_recipe_urls(project_name, server)

    if skip is not None:
        print('FSLCONDA_SKIP_RECIPE_UPDATE is set - aborting recipe update.')
        sys.exit(0)

    if not is_valid_project_version(project_ver):
        print(f'WARNIKNG: {project_ver} is not a valid FSL version!')

    all_pkgs   = get_channel_packages(conda_channel)
    dependants = set()
    for recipe in recipes:
        dependants = dependants.union(find_dependant_packages(
            recipe['name'], all_pkgs))
    dependants = sorted(dependants)

    if mrmsg is None:
        mrmsg = tw.dedent(f"""
        This merge request was triggered by a new tag being added to
        the `{project_path}` project.

        Accepting this merge request will cause the conda recipe for
        `{project_path}` to be updated to version `{project_ver}`.

        If all goes well, a new conda package will built for
        `{project_path}` (`{project_ver}`), and will be uploaded to
        the FSL conda channel.
        """).strip()

        if len(dependants) > 0:
            mrmsg += tw.dedent(f"""

            The following projects are dependent on `{project_path}` - it
            may be necessary to update the `{project_path}` version in the
            conda recipes for these projects:
            """)
            for d in dependants:
                mrmsg += f' - [ ] {d}\n'
            mrmsg += tw.dedent(f"""
            You can do this by manually updating each recipe, or by running
            the `update-dependant-recipes` CI job (part of the "deploy" stage)
            on one of the `{project_path}` conda recipe repository/
            repositories:
            """)
            for r in recipes:
                mrmsg += f' - {r["url"]}\n'

        mrmsg += tw.dedent(f"""

        (MR automatically generated by fsl-ci-rules running on the
        `{project_path}` repository).
        """)

    mrurls = []

    for recipe in recipes:

        print(f'Updating {recipe["path"]}')

        recipe['id'] = lookup_project_id( recipe['path'], server, token)
        defbranch    = get_default_branch(recipe['path'], server, token)
        branch       = gen_branch_name(f'rel/{project_ver}',
                                       recipe['path'],
                                       server,
                                       token)

        if check_mr_already_open(
                server, token, recipe['id'], branch, defbranch):
            print(f'A merge request from branch {branch} is '
                  f'already open on {recipe["url"]} - aborting!')
            continue

        checkout_and_update_recipe(recipe,
                                   defbranch,
                                   branch,
                                   project_path,
                                   project_ver,
                                   build_num)

        mrurls.append(open_merge_request(recipe["path"],
                                         branch,
                                         mrmsg,
                                         server,
                                         token))

    if return_mrurl: return mrurls
    else:            return None


if __name__ == '__main__':
    main()
