#!/usr/bin/env python
#
# versioning.py - Functions for working with FSL project versions.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import string
import datetime

from typing import Tuple


def is_valid_project_version(version : str) -> bool:
    """Return True if the given version/tag is "valid" - it must
    be a sequence of integers, separated by periods, with an optional
    leading 'v'.
    """
    if version.lower().startswith('v'):
        version = version[1:]

    for part in version.split('.'):
        if not all(c in string.digits for c in part):
            return False
    return True


def generate_version() -> str:
    """Generates a FSL project version string based on the current date. """
    version = datetime.date.today().strftime('%y%m')
    return f'{version}.0'


def parse_version(ver : str) -> Tuple[int, ...]:
    """Parses as much of a version string as possible in a very primitive
    manner.
    """
    ver  = ver.lstrip('v')
    bits = ver.split('.')
    nums = []

    for bit in bits:
        try:
            nums.append(int(bit))
        except Exception:
            break
    return nums


def generate_devrelease_version(
        version : str,
        gitref  : str = None,
        time    : str = None) -> str:
    """Generates a version string for a development version of a FSL conda
    package.

    Assuming that the original package version is compliant, a
    conda/PEP440-compliant version string is generated.

    A timestamp is appended to the last version, and `dev0` is appended,
    indicating that it is a development version.

    As an example, if the most recent tagged release of a package is "2111.0",
    a development release built at 14:45 on the 10th of February 2022 would be
    given the version string "2111.0.202202101445.dev0". The timestamp is
    specified relative to UTC.

    If a gitref is provided, it is appended to the end of the string, e.g.:
    "2111.0.202202101445.dev0+ab23f189"

    See https://docs.conda.io/projects/conda/en/latest/user-guide/concepts/pkg-specs.html#version-ordering

    :arg version: Version string of last stable release
    :arg gitref:  Git hash of development version that is being built.
    :arg time:    YYYYMMDDHHMM timestamp of development version. Generated
                  from the current time if not provided.
    """

    if time in (None, ''):
        time = datetime.datetime.utcnow().strftime('%Y%m%d%H%M')

    if gitref is not None: gitref = f'+{gitref[:7]}'
    else:                  gitref = ''

    return f'{version}.{time}.dev0{gitref}'
