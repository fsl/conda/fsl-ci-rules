#!/usr/bin/env bash

if [ ${CUDA_VER} = "11.0" ]; then
  url=https://developer.download.nvidia.com/compute/cuda/11.0.3/local_installers/cuda_11.0.3_450.51.06_linux.run
elif [ ${CUDA_VER} = "11.8" ]; then
  url=https://developer.download.nvidia.com/compute/cuda/11.8.0/local_installers/cuda_11.8.0_520.61.05_linux.run
else
  echo "CUDA version unrecognised: ${CUDA_VER}"
  exit 1
fi

args=()
args+=("--silent")
args+=("--toolkit")
args+=("--override")
args+=("--no-opengl-libs")
args+=("--no-man-page")
args+=("--no-drm")
args+=("--toolkitpath=${CUDA_HOME}")

wget -O ./cuda_runfile.sh --quiet ${url}
sh      ./cuda_runfile.sh "${args[@]}"
rm      ./cuda_runfile.sh
