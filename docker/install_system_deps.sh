#!/usr/bin/env bash

set -e

# Install:
#  - basics
#  - tools to mount nfs4 shares
#  - mesa-libGL, as pyopengl cannot be
#    installed unless libGL is present
#
# The --allowerasing flag is added to work around an issue with the
# curl packages:
#
#   https://www.jeffgeerling.com/blog/2024/fixing-curl-install-failures-ansible-on-red-hat-derivative-oses
dnf install \
    --allowerasing -y \
    wget \
    bc \
    bzip2 \
    tar \
    curl \
    which \
    util-linux-ng \
    nfs-utils \
    mesa-libGL \
    glibc-locale-source \
    glibc-langpack-en

# Make sure en_GB is installed
localedef -i en_GB -f UTF-8 C.UTF-8
