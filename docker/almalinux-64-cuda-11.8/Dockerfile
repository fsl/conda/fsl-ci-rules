# This image is used for linux-64/cuda 11.8
# builds of FSL packages.

FROM --platform=linux/amd64 almalinux:9
LABEL maintainer="FSL development team <https://fsl.fmrib.ox.ac.uk/>"

ENV CUDA_VER="11.8"
ENV CUDA_HOME="/usr/local/cuda"
ENV PATH="/micromamba/bin:/usr/local/cuda/bin/:${PATH}"
ENV NVCC="/usr/local/cuda/bin/nvcc"

COPY /install_system_deps.sh /install_system_deps.sh
COPY /install_miniconda.sh   /install_miniconda.sh
COPY /install_cudatoolkit.sh /install_cudatoolkit.sh
COPY /entrypoint             /fsl-ci-rules-entrypoint

RUN /bin/bash /install_system_deps.sh
RUN /bin/bash /install_miniconda.sh
RUN /bin/bash /install_cudatoolkit.sh

ENTRYPOINT [ "/micromamba/bin/tini", "--", "/fsl-ci-rules-entrypoint" ]
CMD [ "/bin/bash" ]